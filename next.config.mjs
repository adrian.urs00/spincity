/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'www.cmsbetconstruct.com',
        pathname: '/content/**',
      },
    ],
  },
};

export default nextConfig;
