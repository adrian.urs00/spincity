import { GameCard } from '@/components';
import request from '@/lib/request';
import type { Game } from '@/lib/types';

const getQuery = (
  currentCategory?: string,
  currentSearch?: string,
  page?: string
) => {
  if (!currentCategory && !currentSearch && !page) return;

  let query = '?';

  if (currentCategory) query += `cat=${currentCategory}`;
  if (currentSearch)
    query += `${query.includes('=') ? '&' : ''}ser=${currentSearch}`;
  if (page) query += `${query.includes('=') ? '&' : ''}page=${page}`;

  return query;
};

export async function Games({
  ser,
  cat,
  page,
}: {
  ser?: string;
  cat?: string;
  page?: string;
}) {
  const query = getQuery(cat, ser, page);
  console.log(query);
  const games = await request
    .get(`games${query ?? ''}`, { cache: 'no-cache' })
    .json<Game[]>();

  console.log(games);
  return (
    <>
      {games.map((game) => (
        <GameCard key={game.id} game={game} />
      ))}
    </>
  );
}
