'use client';

import { SearchBar } from '@/components';
import { CATEGORIES } from '@/lib/constants';
import { useDebounce } from '@/lib/hooks';
import type { Category } from '@/lib/types';
import { usePathname, useSearchParams, useRouter } from 'next/navigation';
import { useEffect, useState } from 'react';

const allCategoriesOption = {
  id: '',
  title: 'All',
  type: '',
} as const;

type AllCategoriesOption = typeof allCategoriesOption;

export const Search = () => {
  const searchParams = useSearchParams();
  const pathname = usePathname();
  const { replace } = useRouter();

  const prevCat = searchParams.get('cat');
  const [category, setCategory] = useState<Category | AllCategoriesOption>(
    CATEGORIES.find((cat) => cat.id === prevCat) || allCategoriesOption
  );
  const [search, setSearch] = useState(searchParams.get('ser'));

  const debouncedSearch = useDebounce(search, 300);

  function handleSearch(search: string, type: 'cat' | 'ser') {
    const params = new URLSearchParams(searchParams);

    if (params.has(type, search)) return;

    if (search) {
      params.set(type, search);
    } else {
      params.delete(type);
    }

    replace(`${pathname}?${params.toString()}`);
  }

  useEffect(() => {
    handleSearch(category.id, 'cat');
    //eslint-disable-next-line
  }, [category]);

  useEffect(() => {
    handleSearch(debouncedSearch ?? '', 'ser');
    //eslint-disable-next-line
  }, [debouncedSearch]);

  return (
    <div className='w-full !max-w-[600px]'>
      <SearchBar
        selectProps={{
          selected: category,
          onSelect: (option: Category | AllCategoriesOption) =>
            setCategory(option),
          options: [allCategoriesOption as any].concat(CATEGORIES),
          extractLabel: (value: Category | AllCategoriesOption) => {
            return value?.title;
          },
        }}
        inputProps={{
          search: search ?? '',
          onSearch: setSearch,
          placeholder: 'Search for your favourite game...',
        }}
      />
    </div>
  );
};
