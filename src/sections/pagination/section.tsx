'use client';
import { Pagination as PaginationComponent } from '@/components';
import { usePathname, useSearchParams, useRouter } from 'next/navigation';
import { useEffect, useState } from 'react';
import { useGamesMetadata } from './use-games-metadata';

interface PaginationProps {
  ser: string;
  cat: string;
}

export const Pagination = ({ ser, cat }: PaginationProps) => {
  const searchParams = useSearchParams();
  const pathname = usePathname();
  const { replace } = useRouter();
  const { loading, metadata } = useGamesMetadata(ser, cat);

  const prevPage = Number(searchParams.get('page'));
  const [currentPage, setCurrentPage] = useState(
    isNaN(prevPage) ? 1 : prevPage || 1
  );

  useEffect(() => {
    const params = new URLSearchParams(searchParams);

    if (params.has('page', currentPage.toString())) return;

    params.set('page', currentPage.toString());
    replace(`${pathname}?${params.toString()}`);
    //eslint-disable-next-line
  }, [currentPage]);

  if (loading) return <span>Loading...</span>;

  return (
    metadata && (
      <PaginationComponent
        current={currentPage}
        onChange={(newPage) => setCurrentPage(newPage)}
        total={metadata.total_length}
        itemsPerPage={metadata.items_per_page}
      />
    )
  );
};
