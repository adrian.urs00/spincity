import request from '@/lib/request';
import { Meta } from '@/lib/types';
import { useEffect, useState } from 'react';

const getQuery = (currentCategory?: string, currentSearch?: string) => {
  if (!currentCategory && !currentSearch) return;

  let query = '?';

  if (currentCategory) query += `cat=${currentCategory}`;
  if (currentSearch)
    query += `${query.includes('=') ? '&' : ''}ser=${currentSearch}`;

  return query;
};

export function useGamesMetadata(ser?: string, cat?: string) {
  const [metadata, setMetadata] = useState<Meta>();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    (async () => {
      setLoading(true);
      try {
        const query = getQuery(cat, ser);
        const result = await request
          .get(`games/meta${query}`, { cache: 'no-cache' })
          .json<Meta>();
        setMetadata(result);
      } finally {
        setLoading(false);
      }
    })();
  }, [ser, cat]);

  return { metadata, loading };
}
