import { ArrowLeftIcon, ArrowRightIcon } from '@heroicons/react/20/solid';

interface PaginationProps {
  total: number;
  itemsPerPage: number;
  current: number;
  onChange: (newPage: number) => void;
}

export const Pagination = ({
  total,
  itemsPerPage,
  current,
  onChange,
}: PaginationProps) => {
  const totalNumberOfPages = Math.ceil(total / itemsPerPage);
  console.log(totalNumberOfPages);
  const pages = new Array(
    totalNumberOfPages < 5
      ? totalNumberOfPages
      : totalNumberOfPages - current > 5
        ? 5
        : totalNumberOfPages - current
  )
    .fill('')
    .map((_, index) => (current + index).toString());

  return (
    <nav>
      <ul className='flex h-10 items-center -space-x-px text-base'>
        <li>
          <button
            disabled={current === 1}
            onClick={() => onChange(current - 1)}
            className='ms-0 flex h-10 w-11 items-center justify-center rounded-s-lg border border-e-0 border-gray-700  bg-gray-800 leading-tight text-gray-400 enabled:hover:bg-gray-700 enabled:hover:text-white disabled:opacity-50'
          >
            <span className='sr-only'>Previous</span>
            <ArrowLeftIcon className='h-6 w-6 rtl:rotate-180' />
          </button>
        </li>
        {pages.map((page) => (
          <li key={`pagination-page-${page}`}>
            <button
              onClick={() => onChange(Number(page))}
              className={`flex h-10 w-10 items-center justify-center border border-gray-700 ${page === current.toString() ? 'bg-gray-700 hover:text-white' : ' bg-gray-800  text-gray-400'} leading-tight hover:bg-gray-700 hover:text-white`}
            >
              {page}
            </button>
          </li>
        ))}
        <li>
          <button
            disabled={current === totalNumberOfPages}
            onClick={() => onChange(current + 1)}
            className='ms-0 flex h-10 w-10 items-center justify-center rounded-r-lg border border-e-0 border-gray-700  bg-gray-800 leading-tight text-gray-400 enabled:hover:bg-gray-700 enabled:hover:text-white disabled:opacity-50'
          >
            <span className='sr-only'>Next</span>
            <ArrowRightIcon className='h-6 w-6 rtl:rotate-180' />
          </button>
        </li>
      </ul>
    </nav>
  );
};
