'use client';

import Image from 'next/image';
import { ComponentProps, ReactEventHandler, useState } from 'react';
import FallBackImage from '../../../public/assets/default-fallback-image.png';

export const ImageWithFallback = ({
  src,
  ...imageProps
}: ComponentProps<typeof Image>) => {
  const [image, setImage] = useState(src);

  const handleError: ReactEventHandler<HTMLImageElement> = (e) => {
    setImage(FallBackImage.src);
    imageProps.onError?.(e);
  };

  return (
    image && (
      <Image
        {...imageProps}
        src={image}
        onError={handleError}
        alt={imageProps.alt ?? 'image'}
      />
    )
  );
};
