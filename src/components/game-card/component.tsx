import type { Game } from '@/lib/types';
import { ImageWithFallback } from '../image-with-fallback';

export const GameCard = ({ game }: { game: Game }) => {
  return (
    <div className='group flex max-w-52 cursor-pointer flex-col gap-4'>
      <div className='relative h-44 w-52 min-w-52 bg-red-100'>
        <ImageWithFallback
          className='h-44 w-52 object-cover'
          src={game.icon_2}
          alt={game.name}
          width={208}
          height={176}
        />
        <div className='absolute inset-x-0 bottom-0 z-20 flex h-44 w-52 items-center justify-center bg-gray-700 bg-opacity-80 p-5 text-center text-xl font-semibold text-white opacity-0 duration-300 group-hover:opacity-100'>
          Play {game.name}
        </div>
      </div>
      <a className='group-hover:underline'>{game.name}</a>
    </div>
  );
};
