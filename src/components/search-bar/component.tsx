import {
  Listbox,
  ListboxButton,
  ListboxOption,
  ListboxOptions,
} from '@headlessui/react';
import {
  CheckIcon,
  ChevronDownIcon,
  MagnifyingGlassIcon,
} from '@heroicons/react/20/solid';
import { keyGenerator } from '@/lib/utils';

interface SearchBarProps {
  selectProps: {
    selected?: any;
    onSelect?: (option: any) => void;
    options: any;
    extractLabel: (option: any) => string;
  };
  inputProps: {
    placeholder?: string;
    search?: string;
    onSearch?: (value: string) => void;
  };
}

export const SearchBar = ({ selectProps, inputProps }: SearchBarProps) => {
  return (
    <div className='flex h-10 w-full gap-3'>
      <Listbox value={selectProps.selected} onChange={selectProps?.onSelect}>
        <div className='relative h-full w-[20%]'>
          <ListboxButton className='relative h-full w-full cursor-default rounded-lg bg-white py-2 pl-3 pr-10 text-left shadow-md focus:outline-none focus-visible:border-indigo-500 focus-visible:ring-2 focus-visible:ring-white/75 focus-visible:ring-offset-2 focus-visible:ring-offset-orange-300 sm:text-sm'>
            <span className='block truncate text-black'>
              {selectProps.extractLabel(selectProps.selected)}
            </span>

            <span className='pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2'>
              <ChevronDownIcon
                className='h-5 w-5 text-gray-400'
                aria-hidden='true'
              />
            </span>
          </ListboxButton>

          <ListboxOptions className='absolute z-50 mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black/5 focus:outline-none sm:text-sm'>
            {selectProps.options.map((option: any) => (
              <ListboxOption
                key={option.id ?? keyGenerator(10)}
                value={option}
                className={() =>
                  `relative cursor-default select-none py-2 pl-10 pr-4 text-gray-900 hover:bg-amber-100 hover:text-amber-900 `
                }
              >
                {({ selected }) => (
                  <>
                    <span>{selectProps.extractLabel(option)}</span>
                    {selected && (
                      <span className='absolute inset-y-0 left-0 flex items-center pl-3 text-amber-600'>
                        <CheckIcon className='h-5 w-5' aria-hidden='true' />
                      </span>
                    )}
                  </>
                )}
              </ListboxOption>
            ))}
          </ListboxOptions>
        </div>
      </Listbox>
      <div className='relative flex h-full w-full items-center overflow-hidden rounded-lg bg-white'>
        <div className='grid w-12 place-items-center text-gray-300'>
          <MagnifyingGlassIcon className='h-5 w-5' />
        </div>

        <input
          value={inputProps.search}
          onChange={(e) => inputProps.onSearch?.(e.target.value)}
          className='peer w-full pr-2 text-sm text-gray-700 outline-none'
          type='text'
          placeholder={inputProps.placeholder ?? 'Search something..'}
        />
      </div>
    </div>
  );
};
