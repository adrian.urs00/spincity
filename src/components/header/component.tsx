export function Header() {
  return (
    <header className='header flex items-center justify-center'>
      <h1 className='text-5xl font-bold'>SPINCITY</h1>
    </header>
  );
}
