export const CATEGORIES = [
  { id: '51', title: 'Video Slots', type: 'category' },
  { id: '93', title: 'Top Slots', type: 'category' },
  { id: '40', title: 'Video Poker', type: 'category' },
  { id: '95', title: 'Popular Games', type: 'category' },
  { id: '36', title: 'Skill Games', type: 'category' },
  { id: '52', title: 'Mini Games', type: 'category' },
  { id: '28', title: 'Live Dealer', type: 'category' },
  { id: '94', title: 'Table Games', type: 'category' },
  { id: '65', title: 'New', type: 'category' },
  { id: '44', title: 'Other Games', type: 'category' },
] as const;
