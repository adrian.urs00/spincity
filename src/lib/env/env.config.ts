import mock from './mock.json';

// THIS FILE IS CREATED WITH THE ONLY PURPOSE OF REPLACING THE ENV IN RUNNING THE CHALLENGE, PLEASE IGNORE IT
let mocked = process.env.NODE_ENV === 'production' ? {} : mock;

const pre_validation_env = {
  next_public_api_url:
    //@ts-ignore
    process.env.NEXT_PUBLIC_API_URL ?? mocked.NEXT_PUBLIC_API_URL,
};

const required_variables = ['next_public_api_url'];

// checking if the required values are in the .env file
Object.keys(pre_validation_env).forEach((key) => {
  if (!required_variables.includes(key)) return;

  const isPublic: boolean = key.includes('next_public');

  if (
    !isPublic &&
    !!process.platform &&
    !pre_validation_env[key as keyof typeof pre_validation_env]
  )
    throw new Error(
      `@/lib/env/env.config.ts -> missing enviroment variable for ${key.toUpperCase()}`
    );
  else if (
    isPublic &&
    !pre_validation_env[key as keyof typeof pre_validation_env]
  )
    throw new Error(
      `@/lib/env/env.config.ts -> missing enviroment variable for ${key.toUpperCase()}`
    );
});

const _env = pre_validation_env as {
  [key in keyof typeof pre_validation_env]: string;
};

export { _env };
