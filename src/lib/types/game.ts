import { CATEGORIES } from '../constants';

export type Category = (typeof CATEGORIES)[number];

export type Meta = {
  total_length: number;
  items_per_page: number;
};

export type Game = {
  categories: Category['id'][];
  features: any[];
  themes: any[];
  icons: any[];
  backgrounds: any[];
  id: string;
  server_game_id: string;
  external_game_id?: string;
  front_game_id: string;
  name: string;
  title: null;
  ratio: string;
  status: string;
  provider: string;
  show_as_provider: string;
  provider_title: string;
  game_options: null;
  blocked_countries: null;
  has_age_restriction: number;
  icon_2: string;
  background: string;
  types: {
    realMode: number;
    funMode: number;
  };
  game_skin_id: string;
  cats: Category[];
  feats: any[];
  thms: any[];
  active: string;
};
