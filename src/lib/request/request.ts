import ky from 'ky';
import { _env } from '../env/env.config';

export const request = ky.create({
  prefixUrl: _env.next_public_api_url,
});
