import Search from '@/sections/search';
import Games from '@/sections/games';
import { Suspense } from 'react';
import { Pagination } from '@/sections/pagination';

export default function Home({
  searchParams,
}: {
  searchParams: { ser: string; cat: string; page: string };
}) {
  return (
    <div className='flex h-full w-full flex-col items-center gap-8 p-8'>
      <Search />
      <div className='flex w-full flex-wrap justify-start gap-5'>
        <Suspense
          key={`${searchParams.ser}-${searchParams.cat}-${searchParams.page}`}
          fallback={<p>Loading...</p>}
        >
          <Games {...searchParams} />
        </Suspense>
      </div>
      <Pagination {...searchParams} />
    </div>
  );
}
