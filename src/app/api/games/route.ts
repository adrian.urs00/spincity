import { unstable_noStore } from 'next/cache';
import { NextRequest, NextResponse } from 'next/server';
import { Game } from '@/lib/types';
import collection from './data.json';

export const ITEMS_PER_PAGE = 10;

export const GET = async (req: NextRequest): Promise<NextResponse<Game[]>> => {
  unstable_noStore();

  const searchParams = req.nextUrl.searchParams;
  const page = searchParams.get('page') || 1;
  const cat = searchParams.get('cat');
  const ser = searchParams.get('ser');

  let data: Game[] = collection as Game[];

  if (cat)
    data = data.filter((game) =>
      game.cats.some((category) => category.id === cat)
    );

  if (ser)
    data = data.filter((game) =>
      game.name.toLowerCase().includes(ser.toLowerCase())
    );

  data = data.slice(
    (Number(page) - 1) * ITEMS_PER_PAGE,
    Number(page) * ITEMS_PER_PAGE
  );

  return await new Promise((res) => {
    setTimeout(() => res(NextResponse.json(data)), 2000);
  });
};
