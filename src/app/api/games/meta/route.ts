import { NextRequest, NextResponse } from 'next/server';
import { Meta } from '@/lib/types';
import collection from '../data.json';
import { ITEMS_PER_PAGE } from '../route';
import { unstable_noStore } from 'next/cache';

export const GET = async (req: NextRequest): Promise<NextResponse<Meta>> => {
  unstable_noStore();

  const searchParams = req.nextUrl.searchParams;
  const cat = searchParams.get('cat');
  const ser = searchParams.get('ser');
  let worked = collection;

  if (cat)
    worked = worked.filter((game) =>
      game.cats.some((category) => category.id === cat)
    );

  if (ser)
    worked.filter((game) =>
      game.name.toLowerCase().includes(ser.toLowerCase())
    );

  let data = {} as Meta;

  data.total_length = collection.length;
  data.items_per_page = ITEMS_PER_PAGE;

  return await new Promise((res) => {
    setTimeout(() => res(NextResponse.json(data)), 2000);
  });
};
